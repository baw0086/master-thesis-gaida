import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table
import os
from scipy.stats import linregress
from astropy.io import fits
import elk
from elk.ensemble import EnsembleLC
import elk.utils as u
from astropy.coordinates.name_resolve import NameResolveError
from astroquery.skyview import SkyView
from astropy.coordinates import get_icrs_coordinates
import sys

print('start')

with open('targets/elk targets.txt', 'r') as file:
    lines = file.readlines()
with open('targets/elk targets diameters.txt', 'r') as file:
    diameters = file.readlines()
original_stdout = sys.stdout 
sys.stdout = open('elk results/globular cluster diagnostics.txt', 'w')

for j,i in enumerate(lines):
    target = i.strip()
    target = target.replace("â€“", "-")
    target_safename = target.replace(" ","")
    #directory = f'elk results/{target_safename}'
    print("\n\nNow investigating target", target,'\n')
    try:
        diameter = float(diameters[j])
    except ValueError:
        print('Diameter is not a value, skip this object')
        continue
    radius_deg = diameter / 120
    print("diameter =", diameter, "'")
    if diameter <= 8.8:
        tpf_size = 30
    elif diameter <= 15.9:
        tpf_size = 50
    elif diameter <= 23.0:
        tpf_size = 70
    elif diameter <= 34.2:
        tpf_size = 99
    else:
        print('Radius too big! Skip this object.')
        continue
    try:
        target_coordinates = target
        source_coordinates = get_icrs_coordinates(target)
        print(source_coordinates)
        ra_deg = source_coordinates.ra.deg
        dec_deg = source_coordinates.dec.deg
        coords = f"{ra_deg}, {dec_deg}"
    except NameResolveError:
        print('\n\n',target,"not found. Move to next object.\n\n")
        continue
                
    a = EnsembleLC(output_path="elk results/for diagnostics",
                   identifier=target,
                   location=coords,
                   radius=radius_deg,
                   cutout_size=tpf_size,
                   verbose=True)
    a.create_output_table()
    
    if len(a.lcs) > 0:
        #for sector in range(1,2):
        for sector in range(0,len(a.lcs)):
            lc = a.lcs[sector]
            if hasattr(lc, 'sector'):
                try:
                    lc.diagnose_lc_periodogram(output_path='elk results/', identifier=target)[0]
                except TypeError:
                    print('TypeError for sector',lc.sector,'of',target)
                    continue
                stats = lc.get_stats_using_defaults()
                elk_peakfreqs = stats['peak_freqs']
                elp_peakpowers = stats['power_at_peaks']
                five_powers = sorted(elp_peakpowers, reverse=True)[:5]
                indices = sorted(range(len(elp_peakpowers)), key=lambda i: elp_peakpowers[i], reverse=True)[:5]
                five_freqs = [elk_peakfreqs[i] for i in indices]
                print('periodogram peaks:', five_freqs)
                print('\tpowers:', five_powers)    

sys.stdout = original_stdout
print('Dun Dun Dun Dunnn')