import os
import http
#import lightkurve as lk
from lightkurve import search_tesscut
from lightkurve import DesignMatrix
from lightkurve import DesignMatrixCollection
from lightkurve import RegressionCorrector
from lightkurve import LightCurve
import numpy as np
import re
import sys
from copy import deepcopy
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib import patches
from astroquery.skyview import SkyView
from astropy.coordinates import get_icrs_coordinates
from astropy.coordinates.name_resolve import NameResolveError
from numpy.linalg import LinAlgError
from astropy.coordinates import SkyCoord
from astropy.wcs import *
from astropy import units as u
import astropy.io.fits as pyfits
from io import StringIO
sys.setrecursionlimit(10000)
#import warnings
#warnings.filterwarnings("error")
plt.ion()
#input_coord_split = re.split(r"\s+|,|\s*,\s*", input_coord_string)
#print("dusted")

primary_correction_method = 3
tpf_width_height = 30
additive_pca_num = 3
multiplicative_pca_num = 3
pca_only_num = 3
lowest_dss_contour = 0.5 
sys_threshold_orig = 0.2
max_masked_regions = 30
#plot_index_init = 300
plot_index_fraction = 0.3
bf_threshold = 1.5 
prop_error_flag = True

#vvv
#aperture
pixel_threshold_orig = 2.2 #times the background flux (for background_percentile = 10, 2.4 for 20?)
int_pixel_threshold_orig = 2.4 #same but for integrated flux instead of at plot_index
background_percentile_orig = 10 #percentile determining background for pixel threshold
decrease_pixel_threshold = 0.05 #reduce pixel threshold by this if central pixel is already below threshold
decrease_int_pixel_threshold = 0.05 #same for integrated flux
shift_plot_index = 50 #shift index by this if there are zero or negative flux values
endlength_fraction = 0.1 #only shift plot index to this fraction of time indices since systematics might occur
adjacent_ratio_threshold = 1.5 #ratio of bridge pixel flux to adjacent pixel flux, rejected if higher
fraction_of_central = 1/2 #for gradients in galaxy: include pixels with adjacent pixel above this fraction
max_aperture_fraction = 1/3 #maximum fraction of the tpf that can be part of the aperture
tpf_size_large = 50 #new tpf_width_height if aperture has too many pixels
tpf_size_huge = 99 #next tpf_width_height if aperture still has too many pixels
shift_background_percentile = 5 #shift background percentile by this if threshold is negative
max_background_percentile = 20 #maximum allowed percentile for background
extension_threshold = 9 #minimum aperture to reject single-pixel width extensions
sys_fractions = [0.0362, 0.380, 0.6216, 0.8802]

#mask
#maskout_threshold = 20 #times the mean absolute value of the additive component
#maskend_threshold = 0.8 #fraction of sys_threshold determining end of a mask
min_bkg_length = 2/3 #acceptable unmasked fraction of the background before starting with new sys_threshold
increase_sys_threshold = 0.05 #increase sys_threshold by this when too much was masked out
min_mask_gap = 10 #gaps smaller than this between values above sys_threshold are included in masks
maskextension = 2 #number of timesteps added before and after each mask
spiky_width = 3 #maximum width of spiky systematics for findspiky
sys_onestep = 0.1 #mask out if sudden jump in principal component is at least this high
maskwidth_jump = 3 #mask out at most this many extra steps before/after a sudden jump
min_tpf_length = 0.5 #maximum fraction of the tpf that can be rejected due to nans or all zeros
#^^^


#vvv
#new flood fill including rejecting bridge pixels
def flood_fill_bridge(x, y, visited, row_col_coords_init, threshold, pixel_threshold, central):
    if len(row_col_coords_init) == 0 and tpf.flux[plot_index].value[x][x] < threshold:
        pixel_threshold += -decrease_pixel_threshold
        threshold = background_flux * pixel_threshold
        #print('flux of central pixel is too low, no aperture mask selected')
        print('flux of central pixel is too low, set new pixel threshold to',pixel_threshold)
        flood_fill_bridge(x, y, visited, row_col_coords_init, threshold, pixel_threshold, central)
    elif (0 <= x < len(visited[0])
        and 0 <= y < len(visited)
        and not visited[y][x]
        and tpf.flux[plot_index].value[y][x] > threshold):
        # Calculate the direction from the central pixel to the current pixel
        direction_x = x - central
        direction_y = y - central
        # Calculate the adjacent pixel coordinates in the direction of the central pixel
        adj_x = x - (0 if abs(direction_x) < abs(direction_y) else 1 if direction_x > 0 else -1 if direction_x < 0 else 0)
        adj_y = y - (0 if abs(direction_y) < abs(direction_x) else 1 if direction_y > 0 else -1 if direction_y < 0 else 0)
        # Check if the adjacent pixel is within bounds
        if 0 <= adj_x < len(visited[0]) and 0 <= adj_y < len(visited): #take care of edge pixels later
            if x == central and y == central:
                visited[y][x] = True
            # Check if the current pixel's flux is not more than 1 sigma higher than the adjacent pixel's flux
            #if tpf[y, x] <= tpf[adj_y, adj_x] + sigma or tpf[adj_y, adj_x] >= tpf[central, central] - sigma:
            if (visited[adj_y][adj_x]
                and ( tpf.flux[plot_index].value[y][x] <=  tpf.flux[plot_index].value[adj_y][adj_x] * adjacent_ratio_threshold
                or tpf.flux[plot_index].value[adj_y][adj_x] >= tpf.flux[plot_index].value[central][central] * fraction_of_central)):
                visited[y][x] = True
                #print('included (',y,',',x,') with flux',tpf[y,x],'compared to (',adj_y,',',adj_x,') with flux',tpf[adj_y,adj_x])
                row_col_coords_init.append((x, y))
                # Recursively check neighbouring pixels
                flood_fill_bridge(x + 1, y, visited, row_col_coords_init, threshold, pixel_threshold, central)
                flood_fill_bridge(x - 1, y, visited, row_col_coords_init, threshold, pixel_threshold, central)
                flood_fill_bridge(x, y + 1, visited, row_col_coords_init, threshold, pixel_threshold, central)
                flood_fill_bridge(x, y - 1, visited, row_col_coords_init, threshold, pixel_threshold, central)
                
#same as flood_fill_bridge, but for integrated flux values
def flood_fill_integrated(x, y, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central):
    if len(row_col_coords_init) == 0 and integrated_flux[x][x] < int_threshold:
        """int_pixel_threshold += -decrease_int_pixel_threshold
        int_threshold = int_background_flux * int_pixel_threshold
        #print('flux of central pixel is too low, no aperture mask selected')
        print('flux of central pixel is too low (',integrated_flux[x][x],'<',int_threshold,'), set new integrated pixel threshold to',int_pixel_threshold)
        flood_fill_integrated(x, y, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)"""
        print('flux of central pixel is too low (',integrated_flux[x][x],'<',int_threshold,'), instead try non-integrated flood fill.')
        flood_fill_bridge(x, y, visited, row_col_coords_init, threshold, pixel_threshold, central)
    elif (0 <= x < len(visited[0])
        and 0 <= y < len(visited)
        and not visited[y][x]
        and integrated_flux[y][x] > int_threshold):
        # Calculate the direction from the central pixel to the current pixel
        direction_x = x - central
        direction_y = y - central
        # Calculate the adjacent pixel coordinates in the direction of the central pixel
        adj_x = x - (0 if abs(direction_x) < abs(direction_y) else 1 if direction_x > 0 else -1 if direction_x < 0 else 0)
        adj_y = y - (0 if abs(direction_y) < abs(direction_x) else 1 if direction_y > 0 else -1 if direction_y < 0 else 0)
        # Check if the adjacent pixel is within bounds
        if 0 <= adj_x < len(visited[0]) and 0 <= adj_y < len(visited): #take care of edge pixels later
            if x == central and y == central:
                visited[y][x] = True
            # Check if the current pixel's flux is not more than 1 sigma higher than the adjacent pixel's flux
            #if tpf[y, x] <= tpf[adj_y, adj_x] + sigma or tpf[adj_y, adj_x] >= tpf[central, central] - sigma:
            if (visited[adj_y][adj_x]
                and (integrated_flux[y][x] <=  integrated_flux[adj_y][adj_x] * adjacent_ratio_threshold
                or integrated_flux[adj_y][adj_x] >= integrated_flux[central][central] * fraction_of_central)):
                visited[y][x] = True
                #print('included (',y,',',x,') with flux',tpf[y,x],'compared to (',adj_y,',',adj_x,') with flux',tpf[adj_y,adj_x])
                row_col_coords_init.append((x, y))
                # Recursively check neighbouring pixels
                flood_fill_integrated(x + 1, y, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)
                flood_fill_integrated(x - 1, y, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)
                flood_fill_integrated(x, y + 1, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)
                flood_fill_integrated(x, y - 1, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)

#for aperture mask only around the central object
def flood_fill(x, y, visited, row_col_coords_init, threshold, pixel_threshold):
    if len(row_col_coords_init) == 0 and tpf.flux[plot_index].value[x, x] < threshold:
        pixel_threshold += -decrease_pixel_threshold
        threshold = pixmin * pixel_threshold
        print('flux of central pixel is too low, set new pixel threshold to',pixel_threshold)
        flood_fill(x, y, visited, row_col_coords_init, threshold, pixel_threshold)
    elif (
        0 <= x < len(visited[0])
        and 0 <= y < len(visited)
        and not visited[y][x]
        and tpf.flux[plot_index].value[y, x] > threshold
    ):
        visited[y][x] = True
        row_col_coords_init.append((x, y))
        flood_fill(x + 1, y, visited, row_col_coords_init, threshold, pixel_threshold)
        flood_fill(x - 1, y, visited, row_col_coords_init, threshold, pixel_threshold)
        flood_fill(x, y + 1, visited, row_col_coords_init, threshold, pixel_threshold)
        flood_fill(x, y - 1, visited, row_col_coords_init, threshold, pixel_threshold) 
#        else: print('could not select an aperture')

#fill any gaps (one or pixels wide) in the aperture mask
def flood_fill_gaps(x, y, row_col_coords_init, visited_new):
    if 0 < x < len(visited[0])-1 and 0 < y < len(visited)-1:
        if not visited_new[y][x]:
            if not visited[y][x]: 
                if (visited[y-1][x] and visited[y+1][x]) or (visited[y][x-1] and visited[y][x+1]):
                    row_col_coords_init.append((x, y))
                    #print('added (',y,',',x,')')
                elif 1 < x < len(visited[0])-2 and 1 < y < len(visited)-2:
                    if (visited[y-2][x] and visited[y+1][x]) or \
                        (visited[y-1][x] and visited[y+2][x]) or \
                        (visited[y][x-2] and visited[y][x+1]) or \
                        (visited[y][x-1] and visited[y][x+2]):
                        row_col_coords_init.append((x, y))
            visited_new[y][x] = True
            #print('checked (',x,',',y,')')
            flood_fill_gaps(x + 1, y, row_col_coords_init, visited_new)
            flood_fill_gaps(x - 1, y, row_col_coords_init, visited_new)
            flood_fill_gaps(x, y + 1, row_col_coords_init, visited_new)
            flood_fill_gaps(x, y - 1, row_col_coords_init, visited_new)
        

######## BEGIN MAIN PROGRAM ########
#Define target and obtain DSS image from common name or coordinates.

#vvv
original_stdout = sys.stdout 
sys.stdout = open('quaver_output/quaver_apertures_info.txt', 'w')

print('start')

with open('targets/targetlist quaver apertures.txt', 'r') as file:
    lines = file.readlines()

for i in lines:
        #to save printed outputs to a file
    target = i.strip()
    target = target.replace("â€“", "-")
    target_safename = target.replace(" ","")
    #print("\n\nNow investigating target", target,'\n')
    directory = f'quaver_output/{target_safename}'
    if not os.path.exists(directory):
        os.makedirs(directory)
    
    try:
#^^^
        target_coordinates = target
        source_coordinates = get_icrs_coordinates(target)       #this requires that SIMBAD be up and working...
        #print(source_coordinates)
        #print("\n")
    except NameResolveError:
        print('\n\n',target,"not found. Move to next object.\n\n")
        continue

    try:
        dss_image = SkyView.get_images(position=source_coordinates,survey='DSS',pixels=str(400))
        wcs_dss = WCS(dss_image[0][0].header)
        dss_pixmin = np.min(dss_image[0][0].data)
        dss_pixmax = np.max(dss_image[0][0].data)
        dss_pixmean = np.mean(dss_image[0][0].data)
        dss_head = dss_image[0][0].header
        dss_ra = dss_head['CRVAL1']
        dss_dec = dss_head['CRVAL2']
    except IndexError:
        dss_image = SkyView.get_images(position=source_coordinates,survey='TESS',pixels=str(400))
        wcs_dss = WCS(dss_image[0][0].header)
        dss_pixmin = np.min(dss_image[0][0].data)
        dss_pixmax = np.max(dss_image[0][0].data)
        dss_pixmean = np.mean(dss_image[0][0].data)
        dss_head = dss_image[0][0].header
        dss_ra = dss_head['CRVAL1']
        dss_dec = dss_head['CRVAL2']
        print('IndexError! Use TESS survey instead of DSS for contours.')

    #Retrieve the available tesscut data for FFI-only targets.
    sector_data = search_tesscut(target_coordinates)
    num_obs_sectors = len(sector_data)
    if num_obs_sectors == 0:
        print("This object has not been observed by TESS.")
        #sys.exit()
        continue

    list_observed_sectors = []
    #list_observed_sectors_in_cycle = []
    #list_sectordata_index_in_cycle = []
    for i in range(0,len(sector_data)):
        sector_number = int(sector_data[i].mission[0][12:14])       #This will need to change, Y2K style, if TESS ever has more than 100 sectors.
        list_observed_sectors.append(sector_number)

#vvv
    list_observed_sectors.sort()
    sectors_cyc1 = [sec for sec in list_observed_sectors if sec <= 13]
    sectors_cyc2 = [sec for sec in list_observed_sectors if 14 <= sec <= 26]
    sectors_cyc3 = [sec for sec in list_observed_sectors if 27 <= sec <= 39]
    sectors_cyc4 = [sec for sec in list_observed_sectors if 40 <= sec <= 55]
    sectors_cyc5 = [sec for sec in list_observed_sectors if 56 <= sec <= 69]
    sectors_cyc6 = [sec for sec in list_observed_sectors if 70 <= sec]
    #print('observed sectors for',target.strip(),':',list_observed_sectors)
#^^^

    #print('\nstarting analysis')
    #If object is observed by TESS and specified Cycle makes sense, begin aperture selection and extraction!

    #vvv    
    #for current_sector in range(3,4):
    for current_sector in range(len(list_observed_sectors)):
        try:
    #^^^
            tpf = sector_data[current_sector].download(cutout_size=(tpf_width_height, tpf_width_height)) #gets earliest sector
    #vvv
        except Exception:
            print('\n\nSearchError for',target,': Unable to download FFI cutout. Desired target coordinates may be too near the edge of the FFI. Move to next object.\n\n')
            continue

        #print(integrated_flux)
        #input('ok')
        plot_index = round(len(tpf.flux) * plot_index_fraction)
        central = tpf_width_height // 2
        if current_sector <= 26:
            cadencefactor = 1
        elif current_sector <= 55:
            cadencefactor = 3
        else:
            cadencefactor = 9
            
        #instead of loop:
        orig_length = len(tpf)
        tpf = tpf[~np.isnan(tpf.flux).any(axis=(1,2)) & ~(tpf.flux == 0).all(axis=(1,2))]
        length = len(tpf)
    #^^^
        sector_number = tpf.get_header()['SECTOR']
        sec = str(sector_number)
        ccd = tpf.get_header()['CCD']
        cam = tpf.get_header()['CAMERA']
    #vvv
        if length < orig_length * min_tpf_length:
            print('Too many cadences have NaNs or are all zero (',orig_length-length,'of',orig_length,'). Skip this sector.')
            continue
    #^^^
        #print("\nGenerating pixel map for sector "+sec+".")

        #Check that this object is actually on silicon and getting data (not always the case just because TESSCut says so).
        #By making a light curve from a dummy aperture of the middle 5x5 square and seeing if its mean flux is zero.
        aper_dummy = np.zeros(tpf[0].shape[1:], dtype=bool) #blank
        aper_dummy[int(tpf_width_height/2-3):int(tpf_width_height/2+3),int(tpf_width_height/2-3):int(tpf_width_height/2+3)] = True
        lc_dummy = tpf.to_lightcurve(aperture_mask=aper_dummy)

        if np.mean(lc_dummy.flux) == 0:
            print("This object is not actually on silicon.")
            #sys.ext()
            #continue
            break
        else:
            hdu = tpf.get_header(ext=2)
            #Get WCS information and flux stats of the TPF image.
            tpf_wcs = WCS(tpf.get_header(ext=2))
            pixmin = np.min(tpf.flux[plot_index]).value
        #vvv
            if orig_length != length:
                print('Removed',orig_length-length,'cadences due to nan values or all zero pixel fluxes.')
            if tpf.flux[plot_index].value[central][central] <= 0:
                while plot_index < length*(1-endlength_fraction):
                    plot_index += shift_plot_index
                    print('Central pixel has zero or negative flux value (',tpf.flux[plot_index].value[central][central],')! Now trying index',plot_index,'.')
                    #print('There are negative or zero flux values (pixmin =',pixmin_init,')! \nNow trying index',plot_index,'.')
                    central_pixel_flux = tpf.flux[plot_index].value[central][central]
                    if central_pixel_flux > 0:
                        print('Flux is',central_pixel_flux,'at plot index',plot_index,'. Use this now.')
                        break
                if central_pixel_flux < 0:
                    #pixmin = np.min(x for x in tpf.flux[plot_index_init] if x > 0).value
                    #print('Could not find index without negative or zero flux values. Instead use smallest nonzero absolute value at index',plot_index_init,'. Resulting aperture might not be reasonable.')   
                    print('Central pixel flux negative at all tested plot indices. Skip this sector.')
                    #continue
                    break #due to insertd while loop
                elif central_pixel_flux == 0:
                    print('Central pixel flux zero at all tested plot indices. Skip this sector.')
                    #continue
                    break
        #^^^
            pixmax = np.max(tpf.flux[plot_index]).value
            pixmean = np.mean(tpf.flux[plot_index]).value
            temp_min = float(pixmin)
            # print(temp_min)
            temp_max = float(1e-3*pixmax+pixmean)
            #temp_max = pixmax
            # print(temp_max)
            #Create a blank boolean array for the aperture, which will turn to TRUE when pixels are selected.
            aper = np.zeros(tpf[0].shape[1:], dtype=bool) #blank
            aper_mod = aper.copy() #For the source aperture
            aper_buffer = aper.copy()    #For the source aperture plus a buffer region to exclude from both additive and mult. regressors
            aper_width = tpf[0].shape[1]

        #vvv
            #threshold = pixmin * pixel_threshold
            central = tpf_width_height // 2
            background_percentile = background_percentile_orig
            pixel_threshold = pixel_threshold_orig
            int_pixel_threshold = int_pixel_threshold_orig

            timelength = tpf.time.value[-1] - tpf.time.value[0]
            sys_times = [tpf.time.value[0] + i * timelength for i in sys_fractions]
            time_indices = []
            for a, b in enumerate(tpf.time[:-2]):
                if any(b.value <= threshold < tpf.time.value[a+1] for threshold in sys_times):
                    time_indices.append(a)
            integrated_flux = np.sum(tpf.flux.value[time_indices[0]:time_indices[1]], axis=0) + np.sum(tpf.flux.value[time_indices[2]:time_indices[3]], axis=0) #'''
            #integrated_flux = np.sum(tpf.flux.value, axis=0)
                #sometimes better to use tpf.flux[plot_index].value instead of background_flux?
            
            background_flux = np.nanpercentile(tpf.flux[plot_index].value, background_percentile)
            int_background_flux = np.nanpercentile(integrated_flux, background_percentile)
            
            flux_flat = np.array(tpf.flux.value).flatten()
            integrated_flux_flat = np.array(integrated_flux).flatten()
            flux_nans = np.isnan(flux_flat).sum()
            int_flux_nans = np.isnan(integrated_flux_flat).sum()
            if flux_nans > 0 or int_flux_nans > 0:
                print('nan values in background!')
                print(flux_nans,'in background flux,',int_flux_nans,'in integrated background flux')
                if int_flux_nans >= tpf_width_height**2*background_percentile/100:
                    print('That is too many! Skip this sector.')
                    break
            
            for x in range(6):
                int_pixel_threshold += 0.05  

                threshold = background_flux * pixel_threshold
                int_threshold = int_background_flux * int_pixel_threshold
                if threshold <= 0 or int_threshold <= 0:
                    while background_percentile <= max_background_percentile:
                        print('Negative threshold! Background flux =',background_flux,', integrated background flux =',int_background_flux)
                        print('Try with background_percentile =',background_percentile)
                        background_flux = np.nanpercentile(tpf.flux[plot_index].value, background_percentile)
                        int_background_flux = np.nanpercentile(integrated_flux, background_percentile)
                        background_percentile += shift_background_percentile
                        if background_flux > 0 and int_background_flux > 0:
                            break
                    else:
                        print('Reached max background percentile, threshold still negative (background_flux =',background_flux,'; int_background_flux =',int_background_flux,').')
                        print('Skip this sector.')
                        break
    
                row_col_coords_init = []
                # Initialise visited matrix
                visited = [[False] * tpf_width_height for _ in range(tpf_width_height)]
                # Perform flood-fill starting from the central pixel
                #flood_fill(central, central, visited, row_col_coords_init, threshold, pixel_threshold)
                visited_new = [[False] * tpf_width_height for _ in range(tpf_width_height)]
                #flood_fill_bridge(central, central, visited, row_col_coords_init, threshold, pixel_threshold, central)
                flood_fill_integrated(central, central, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)
                flood_fill_gaps(central, central, row_col_coords_init, visited_new)
                if len(row_col_coords_init) < 4:
                    print("warning: aperture is only",len(row_col_coords_init),"pixels")
                    """print('automatic aperture too small (',len(row_col_coords_init),'pixels), instead choose 4 pixel square')
                     row_col_coords_init = []
                    row_col_coords_init.append((central, central))
                    row_col_coords_init.append((central - 1, central))
                    row_col_coords_init.append((central, central - 1))
                    row_col_coords_init.append((central - 1, central - 1))""" #not actually a problem
    
                #when aperture too big or has edge pixels
                if len(row_col_coords_init) >= tpf_width_height**2 * max_aperture_fraction or any((0 in px or tpf_width_height-1 in px) for px in row_col_coords_init):
                    if len(row_col_coords_init) >= tpf_width_height**2 * max_aperture_fraction:
                        print('Aperture too big (',len(row_col_coords_init),'pixels)! Increase TPF size to',tpf_size_large,'pixels.')
                    if any((0 in px or tpf_width_height in px) for px in row_col_coords_init):
                        print('There are edge pixels in the aperture. Increase TPF size to',tpf_size_large,'pixels.')
                    try:
                        tpf = sector_data[current_sector].download(cutout_size=(tpf_size_large, tpf_size_large)) #gets earliest sector
                    except SearchError:
                        print('\n\nSearchError for',target,': Unable to download FFI cutout. Desired target coordinates may be too near the edge of the FFI. Move to next object.\n\n')
                        #sys.stdout = original_stdout
                        continue
                    tpf = tpf[~np.isnan(tpf.flux).any(axis=(1,2)) & ~(tpf.flux == 0).all(axis=(1,2))]
                    length = len(tpf)
                    if length < orig_length * min_tpf_length:
                        print('Too many cadences have NaNs or are all zero (',orig_length-length,'of',orig_length,'). Skip this sector.')
                        continue
           
                    central = tpf_size_large // 2
                    #integrated_flux = np.sum(tpf.flux.value, axis=0)
                    integrated_flux = np.sum(tpf.flux.value[time_indices[0]:time_indices[1]], axis=0) + np.sum(tpf.flux.value[time_indices[2]:time_indices[3]], axis=0)
                    background_flux = np.percentile(tpf.flux[plot_index].value, background_percentile)
                    int_background_flux = np.percentile(integrated_flux, background_percentile)
                    threshold = background_flux * pixel_threshold
                    int_threshold = int_background_flux * int_pixel_threshold
                    if threshold <= 0 or int_threshold <= 0:
                        print('Negative threshold! Background flux =',background_flux,', integrated background flux =',int_background_flux)
                        print('Move to next sector.')
                        break
                    row_col_coords_init = []
                    visited = [[False] * tpf_size_large for _ in range(tpf_size_large)]
                    visited_new = [[False] * tpf_size_large for _ in range(tpf_size_large)]
                    #flood_fill_bridge(central, central, visited, row_col_coords_init, threshold, pixel_threshold, central)
                    flood_fill_integrated(central, central, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)
                    flood_fill_gaps(central, central, row_col_coords_init, visited_new)
    
                    #increase tpf size for a second time:
                    if len(row_col_coords_init) >= tpf_width_height**2 * max_aperture_fraction or any((0 in px or tpf_width_height in px) for px in row_col_coords_init):
                        if len(row_col_coords_init) >= tpf_width_height**2 * max_aperture_fraction:
                            print('Still too big (',len(row_col_coords_init),'pixels). Try once more with',tpf_size_huge,'pixels.')
                        if any((0 in px or tpf_width_height in px) for px in row_col_coords_init):
                            print('There are edge pixels in the aperture. Increase TPF size to',tpf_size_huge,'pixels.')
                        try:
                            tpf = sector_data[current_sector].download(cutout_size=(tpf_size_huge, tpf_size_huge)) #gets earliest sector
                        except SearchError:
                            print('\n\nSearchError for',target,': Unable to download FFI cutout. Desired target coordinates may be too near the edge of the FFI. Move to next object.\n\n')
                            #sys.stdout = original_stdout
                            continue
                        tpf = tpf[~np.isnan(tpf.flux).any(axis=(1,2)) & ~(tpf.flux == 0).all(axis=(1,2))]
                        length = len(tpf)
                        if length < orig_length * min_tpf_length:
                            print('Too many cadences have NaNs or are all zero (',orig_length-length,'of',orig_length,'). Skip this sector.')
                            continue                    
                        central = tpf_size_huge // 2
                        #integrated_flux = np.sum(tpf.flux.value, axis=0)
                        integrated_flux = np.sum(tpf.flux.value[time_indices[0]:time_indices[1]], axis=0) + np.sum(tpf.flux.value[time_indices[2]:time_indices[3]], axis=0)
                        background_flux = np.percentile(tpf.flux[plot_index].value, background_percentile)
                        int_background_flux = np.percentile(integrated_flux, background_percentile)
                        threshold = background_flux * pixel_threshold
                        int_threshold = int_background_flux * int_pixel_threshold
                        if threshold <= 0 or int_threshold <= 0:
                            print('Negative threshold! Background flux =',background_flux,', integrated background flux =',int_background_flux)
                            print('Move to next sector.')
                            break
                        row_col_coords_init = []
                        visited = [[False] * tpf_size_huge for _ in range(tpf_size_huge)]
                        visited_new = [[False] * tpf_size_huge for _ in range(tpf_size_huge)]
                        #flood_fill_bridge(central, central, visited, row_col_coords_init, threshold, pixel_threshold, central)
                        flood_fill_integrated(central, central, visited, row_col_coords_init, int_threshold, int_pixel_threshold, central)
                        flood_fill_gaps(central, central, row_col_coords_init, visited_new) #"""
    
                    if len(row_col_coords_init) >= tpf_width_height**2 * max_aperture_fraction or any((0 in px or tpf_width_height in px) for px in row_col_coords_init):
                        if len(row_col_coords_init) >= tpf_width_height**2 * max_aperture_fraction:
                            print('Nope, still too huge (',len(row_col_coords_init),'pixels). Skip this sector.')
                        if any((0 in px or tpf_width_height in px) for px in row_col_coords_init):
                              print('Edge pixels in the aperture. Skip this sector.')
                        continue
                    pixmin = np.min(tpf.flux[plot_index]).value
                    pixmax = np.max(tpf.flux[plot_index]).value
                    pixmean = np.mean(tpf.flux[plot_index]).value
                    temp_min = float(pixmin)
                    temp_max = float(1e-3*pixmax+pixmean)
                    aper = np.zeros(tpf[0].shape[1:], dtype=bool) #blank
                    aper_mod = aper.copy() #For the source aperture
                    aper_buffer = aper.copy()    #For the source aperture plus a buffer region to exclude from both additive and mult. regressors
                    aper_width = tpf[0].shape[1]
    
                #reject single-pixel width extensions more than 2 pixels long
                directions = [(0, 1), (1, 0), (0, -1), (-1, 0)]
                diagonal_directions = [(1, 1), (-1, -1), (1, -1), (-1, 1)]
                if len(row_col_coords_init) >= extension_threshold:
                    for j in range(0,3): #to catch zigzaging pixels
                        pixels_to_remove = []
                        for p in row_col_coords_init:
                            neighbours = [direction for direction in directions if (p[0] + direction[0], p[1] + direction[1]) in row_col_coords_init]
                            if len(neighbours) == 1:
                                pixels_to_remove.append(p)
                                diagonal_neighbours = [(p[0] + neighbours[0][0] + dx, p[1] + neighbours[0][1] + dy) for dx, dy in directions if dx != -neighbours[0][0] and dy != -neighbours[0][1]]
                                if not any(neighbour in row_col_coords_init for neighbour in diagonal_neighbours):
                                    pixels_to_remove.append((p[0] + neighbours[0][0], p[1] + neighbours[0][1])) #remove second pixel
                                    #pixels_to_remove.append(p) #when only removing 2-pixel extensions
                                    ##pixels_to_remove.append((pixel[0] + direct_neighbors[0][0], pixel[1] + direct_neighbors[0][1]))
                                    current_pixel = (p[0] + neighbours[0][0], p[1] + neighbours[0][1])
                                    for px in range(1,tpf_width_height):
                                        diagonal_neighbours = [(current_pixel[0] + neighbours[0][0] + dx, current_pixel[1] + neighbours[0][1] + dy) for dx, dy in directions if dx != -neighbours[0][0] and dy != -neighbours[0][1]]
                                        current_pixel = (current_pixel[0] + neighbours[0][0], current_pixel[1] + neighbours[0][1])
                                        if not any(neighbour in row_col_coords_init for neighbour in diagonal_neighbours):
                                            pixels_to_remove.append(current_pixel)
                                        else:
                                            break
                        row_col_coords_init = [pixel for pixel in row_col_coords_init if pixel not in pixels_to_remove]
                    if (central,central) not in row_col_coords_init:
                        print('Tried to remove central pixel! Skip this sector.')
                        continue #"""
                        
                #mirror pixels to account for original algorithm
                row_col_coords = [(y, x) for x, y in row_col_coords_init]
                #print('aperture mask:', row_col_coords)
                #print('length:',len(row_col_coords),'pixels')
                print(target_safename, sec, len(row_col_coords))
            #^^^
                #Plot the TPF image and the DSS contours together, to help with aperture selection, along with the starter aperture.
                if lowest_dss_contour == 0.4:
                    dss_levels = [0.4*dss_pixmax,0.5*dss_pixmax,0.75*dss_pixmax]
                elif lowest_dss_contour < 0.4:
                    dss_levels = [lowest_dss_contour*dss_pixmax,0.4*dss_pixmax,0.5*dss_pixmax,0.75*dss_pixmax]
                elif lowest_dss_contour > 0.4:
                    dss_levels = [lowest_dss_contour*dss_pixmax,0.65*dss_pixmax,0.85*dss_pixmax]
                fig = plt.figure(figsize=(6,6))
                ax = fig.add_subplot(111,projection=tpf_wcs)
                # ax.imshow(tpf.flux[200],vmin=pixmin,vmax=1e-3*pixmax+pixmean)
                ax.imshow(tpf.flux[plot_index].value,vmin=temp_min,vmax=temp_max)
                ax.contour(dss_image[0][0].data,transform=ax.get_transform(wcs_dss),levels=dss_levels,colors='white',alpha=0.9)
                for pixel in row_col_coords_init:
                    ax.scatter(pixel[0], pixel[1], marker='x', color='red', s=30)
                ax.scatter(aper_width/2.0,aper_width/2.0,marker='x',color='k',s=8)
                ax.set_xlim(-0.5,aper_width-0.5)  #This section is needed to fix the stupid plotting issue in Python 3.
                ax.set_ylim(-0.5,aper_width-0.5)
                directory = str(target).replace(" ","")   
                plt.savefig('quaver_output/'+target_safename+'/'+target_safename+'_TPF_sector_'+sec+'_'+str(int_pixel_threshold)+'.png',format='png')
        
#vvv
    #print("Done with",target)
sys.stdout = original_stdout

print ("Done and Done!")

