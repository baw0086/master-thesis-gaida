import numpy as np
from astropy.table import Table
import os
from astropy.io import fits
import elk
from elk.ensemble import EnsembleLC
from astropy.coordinates.name_resolve import NameResolveError
from astroquery.skyview import SkyView
from astropy.coordinates import get_icrs_coordinates
import sys

print('start')

lines = ['IC 5325']
diameters = ['2.535']

for j,i in enumerate(lines):
    target = i.strip()
    target = target.replace("Ã¢â‚¬â€œ", "-")
    target_safename = target.replace(" ","")
    print("\n\nNow investigating target", target,'\n')
    try:
        diameter = float(diameters[j])
    except ValueError:
        print('Diameter is not a value, skip this object')
        continue
    radius_deg = diameter / 120
    print("diameter =", diameter, "'")
    tpf_size = 30
    try:
        source_coordinates = get_icrs_coordinates(target)
        print(source_coordinates)
        ra_deg = source_coordinates.ra.deg
        dec_deg = source_coordinates.dec.deg
        coords = f"{ra_deg}, {dec_deg}"
    except NameResolveError:
        print('\n\n',target,"not found. Move to next object.\n\n")
        continue
                
    a = EnsembleLC(output_path="elk results",
                   identifier='IC 5325',
                   location=coords,
                   radius=radius_deg,
                   cutout_size=tpf_size,
                   verbose=True)
    a.create_output_table()
    
    sectors = []
    if len(a.lcs) > 0:
        for sector in range(0,len(a.lcs)):
            lc = a.lcs[sector]
            if hasattr(lc, 'sector'):
                sectors.append(lc.sector)
                lc.to_periodogram(frequencies=np.logspace(-1, 1, 500), n_bootstrap=10)
                lc.plot_periodogram(save_path='elk results/Figures/'+target+'_LSP_'+str(lc.sector)+'.png')
                try:
                    lc.diagnose_lc_periodogram(output_path='elk results/Figures/', identifier=target)[0]
                except TypeError:
                    print('TypeError for sector',lc.sector,'of',target)
                    continue

                stats = lc.get_stats_using_defaults()
                elk_peakfreqs = stats['peak_freqs']
                elp_peakpowers = stats['power_at_peaks']
                five_powers = sorted(elp_peakpowers, reverse=True)[:5]
                indices = sorted(range(len(elp_peakpowers)), key=lambda i: elp_peakpowers[i], reverse=True)[:5]
                five_freqs = [elk_peakfreqs[i] for i in indices]
                print('\nsector',lc.sector)
                print('periodogram peaks:', five_freqs)
                print('\tpowers:', five_powers)    
            else:
                sectors.append("??")
    print('sectors:',sectors)

print('Dun Dun Dun Dunnn')




