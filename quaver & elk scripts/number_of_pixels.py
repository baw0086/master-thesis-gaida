#getting number of pixels in aperture
import re
import ast
import csv
directory = "Simbad Imag pca 100"
with open('targets/galaxies right now.txt', 'r') as file:
    reader = csv.reader(file, delimiter='\t') 
    g = []
    s = []
    for row in reader:
        g.append(row[0]) 
        s.append(row[1])
    
filename = "pixels.txt"
messages = []

for i,ga in enumerate(g):
    gal =  ga.strip().replace("–", "-").replace(" ","")
    try:
        #with open('../quaver/results/'+directory+'/'+gal+'/'+gal+'_info_'+str(s[i])+'.txt', 'r') as f:
        #with open('D:/Masterarbeit/quaver/results/'+directory+'/'+gal+'/'+gal+'_info_'+str(s[i])+'.txt', 'r') as f:
        with open('results/'+directory+'/'+gal+'/'+gal+'_info_'+str(s[i])+'.txt', 'r') as f:
            lines = f.readlines()
    except FileNotFoundError as e:
        messages.append(str(e))
        continue
    for line in lines:
        match = re.search(r"(?i)^aperture mask:\s*\[(.*?)\]", line)
        if match:
            list_string = match.group(1)
            try:
                list_from_string = ast.literal_eval(list_string)
                list_length = len(list_from_string)
                message = f"{gal}, sector:{str(s[i])}:{list_length}"
                messages.append(message)
            except (ValueError, SyntaxError):
                print(f"Error parsing list in line: {line}")
            break

message_string = "\n".join(messages)
with open(filename, "a") as file:
    file.write(message_string)