import numpy as np
import matplotlib.pyplot as plt
from astropy.table import Table
import os
from scipy.stats import linregress
from astropy.io import fits
import elk
from elk.ensemble import EnsembleLC
import elk.utils as u
from astropy.coordinates.name_resolve import NameResolveError
from astroquery.skyview import SkyView
from astropy.coordinates import get_icrs_coordinates
import sys
from astroquery.simbad import Simbad

print('start')

with open('targets/elk targets.txt', 'r') as file:
    lines = file.readlines()

for j,i in enumerate(lines):
    target = i.strip()
    target = target.replace("â€“", "-")
    target_safename = target.replace(" ","")
    #directory = f'elk results/{target_safename}'
    print("\n\nNow investigating target", target,'\n')
    original_stdout = sys.stdout 
    sys.stdout = open('elk results/'+target+'_info.txt', 'w')
    try:
        customSimbad = Simbad()
        customSimbad.add_votable_fields('dimensions')
        result = customSimbad.query_object(target)
        try:
            majaxis = result['GALDIM_MAJAXIS']
            minaxis = result['GALDIM_MINAXIS']
        except KeyError:
            print('KeyError when trying to access diameter')
            continue
        if (majaxis is None or not isinstance(majaxis[0], (int, float, np.float32))) and (minaxis is None or not isinstance(minaxis[0], (int, float, np.float32))):
            print('no diameter found, skip this object x')
            continue
        elif (majaxis is None or not isinstance(majaxis[0], (int, float, np.float32))) and (minaxis is not None and isinstance(minaxis[0], (int, float, np.float32))):
            diameter = minaxis[0]
            print("diameter = minaxis =", diameter, "'")
        elif (majaxis is not None and isinstance(majaxis[0], (int, float, np.float32))) and (minaxis is None or not isinstance(minaxis[0], (int, float, np.float32))):
            diameter = majaxis[0]
            print("diameter = majaxis =", diameter, "'")
        else:
            diameter = (majaxis[0] + minaxis[0]) / 2
            print('majaxis =',majaxis[0],"' ; minaxis =",minaxis[0],"'")
            print("diameter =", diameter, "'")
    except TypeError:
        print('no diameter found, skip this object')
        continue    
    radius_deg = diameter / 120
    if diameter <= 8.8:
        tpf_size = 30
    elif diameter <= 15.9:
        tpf_size = 50
    elif diameter <= 23.0:
        tpf_size = 70
    elif diameter <= 34.2:
        tpf_size = 99
    else:
        print('Radius too big! Skip this object.')
        sys.stdout = original_stdout
        continue
    try:
        target_coordinates = target
        source_coordinates = get_icrs_coordinates(target)
        print(source_coordinates)
        ra_deg = source_coordinates.ra.deg
        dec_deg = source_coordinates.dec.deg
        coords = f"{ra_deg}, {dec_deg}"
    except NameResolveError:
        print('\n\n',target,"not found. Move to next object.\n\n")
        sys.stdout = original_stdout
        continue
                
    a = EnsembleLC(output_path="elk results",
                   identifier=target,
                   location=coords,
                   radius=radius_deg,
                   cutout_size=tpf_size,
                   verbose=True)
    a.create_output_table()
    
    sectors = []
    if len(a.lcs) > 0:
        #for sector in range(1,2):
        for sector in range(0,len(a.lcs)):
            lc = a.lcs[sector]
            if hasattr(lc, 'sector'):
                sectors.append(lc.sector)
                lc.to_periodogram(frequencies=np.logspace(-1, 1, 500), n_bootstrap=10)
                lc.plot_periodogram(save_path='elk results/Figures/'+target+'_LSP_'+str(lc.sector)+'.png')
            else:
                sectors.append("??")
    print('sectors:',sectors)
    
    sys.stdout = original_stdout
print('Dun Dun Dun Dunnn')